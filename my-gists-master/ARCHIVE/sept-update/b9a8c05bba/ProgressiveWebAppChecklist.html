<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>ProgressiveWebAppChecklist</title>
  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
  </style>
</head>
<body>
<h2 id="core-progressive-web-app-checklist">Core Progressive Web App checklist </h2>
<p>The Progressive Web App Checklist describes what makes an app installable and usable by all users, regardless of size or input type.</p>
<h3 id="starts-fast-stays-fast">Starts fast, stays fast</h3>
<p>Performance plays a significant role in the success of any online experience, because high performing sites engage and retain users better than poorly performing ones. Sites should focus on optimizing for user-centric performance metrics.</p>
<h4 id="why">Why <a href="https://web.dev/pwa-checklist/#why">#</a></h4>
<p>Speed is critical for getting users to <em>use</em> your app. In fact, as page load times go from one second to ten seconds, the probability of a user bouncing increases by 123%. Performance doesn’t stop with the <code>load</code> event. Users should never wonder whether their interaction—for example, clicking a button—was registered or not. Scrolling and animation should feel smooth. Performance affects your entire experience, from how users perceive your application to how it actually performs.</p>
<p>While all applications have different needs, the performance audits in Lighthouse are based on the <a href="https://developers.google.com/web/fundamentals/performance/rail">RAIL user-centric performance model</a>, and scoring high on those audits will make it more likely that your users have an enjoyable experience. You can also use <a href="https://developers.google.com/speed/pagespeed/insights/">PageSpeed Insights</a> or the <a href="https://developers.google.com/web/tools/chrome-user-experience-report/">Chrome User Experience Report</a> to get real-world performance data for your web app.</p>
<h4 id="how">How <a href="https://web.dev/pwa-checklist/#how">#</a></h4>
<p>Follow our <a href="https://web.dev/fast/">guide on fast load times</a> to learn how to make your PWA start fast and stay fast.</p>
<h3 id="works-in-any-browser">Works in any browser</h3>
<p>Users can use any browser they choose to access your web app before it’s installed.</p>
<h4 id="why-1">Why <a href="https://web.dev/pwa-checklist/#why-2">#</a></h4>
<p>Progressive Web Apps are web apps first, and that means they need to work across browsers, not just in one of them.</p>
<p>An effective way of doing this is by, in the words of Jeremy Keith in <a href="https://resilientwebdesign.com/">Resilient Web Design</a>, identifying the core functionality, making that functionality available using the simplest possible technology, and then enhancing the experience where possible. In many cases, this means starting with just HTML to create the core functionality, and enhancing the user experience with CSS and JavaScript to create a more engaging experience.</p>
<p>Take form submission for example. The simplest way to implement that is an HTML form that submits a <code>POST</code> request. After building that, you can enhance the experience with JavaScript to do form validation and submit the form via AJAX, improving the experience for users who can support it.</p>
<p>Consider that your users will experience your site across a spectrum of devices and browsers. You cannot simply target the top end of the spectrum. By using feature detection, you’ll be able to deliver a usable experience for the widest range of potential users, including those using browsers and devices that may not exist today.</p>
<h4 id="how-1">How <a href="https://web.dev/pwa-checklist/#how-2">#</a></h4>
<p>Jeremy Keith’s <a href="https://resilientwebdesign.com/">Resilient Web Design</a> is an excellent resource describing how to think about web design in this cross-browser, progressive methodology.</p>
<h4 id="additional-reading">Additional reading <a href="https://web.dev/pwa-checklist/#additional-reading">#</a></h4>
<ul>
<li>A List Apart’s <a href="https://alistapart.com/article/understandingprogressiveenhancement/">Understanding Progressive Enhancement</a> is a good introduction to the topic.</li>
<li>Smashing Magazine’s <a href="https://www.smashingmagazine.com/2009/04/progressive-enhancement-what-it-is-and-how-to-use-it/">Progressive Enhancement: What It Is, And How To Use It?</a> gives a practical introduction and links to more advanced topics.</li>
<li>MDN has an article titled <a href="https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Cross_browser_testing/Feature_detection">Implementing feature detection</a> that talks about how to detect a feature by directly querying it.</li>
</ul>
<h3 id="responsive-to-any-screen-size">Responsive to any screen size</h3>
<p>Users can use your PWA on any screen size and all of the content is available at any viewport size.</p>
<h4 id="why-2">Why <a href="https://web.dev/pwa-checklist/#why-3">#</a></h4>
<p>Devices come in a range of sizes, and users may use your application at a range of sizes, even on the same device. Therefore, it’s critical to ensure your content not only fits within the viewport, but that all features and content for your site are usable at all viewport sizes.</p>
<p>The tasks users want to complete and the content they want to access do not change with viewport size. The content can be rearranged at different viewport sizes, and it should all be there, one way or another. In fact, as Luke Wroblewski states in his book Mobile First, starting small and going large instead of the other way around can actually improve a site’s design:</p>
<blockquote>
<p>Mobile devices require software development teams to focus on only the most important data and actions in an application. There simply isn’t room in a 320 by 480 pixel screen for extraneous, unnecessary elements. You have to prioritize.</p>
</blockquote>
<h4 id="how-2">How <a href="https://web.dev/pwa-checklist/#how-3">#</a></h4>
<p>There are many resources on responsive design, including the <a href="https://alistapart.com/article/responsive-web-design/">original article by Ethan Marcotte</a>, a <a href="https://snugug.com/musings/principles-responsive-web-design/">collection of important concepts</a> related to it, as well as books and talks galore. To narrow this discussion down to the content aspects of responsive design, you can dig into <a href="https://uxdesign.cc/why-you-should-design-the-content-first-for-better-experiences-374f4ba1fe3c">content-first design</a> and <a href="https://alistapart.com/article/content-out-layout/">content-out responsive layouts</a>. Finally, while it’s focused on mobile, the lessons in <a href="https://www.forbes.com/sites/anthonykosner/2012/05/03/seven-deadly-mobile-myths-josh-clark-debunks-the-desktop-paradigm-and-more/#21ecac977bca">Seven Deadly Mobile Myths</a> by Josh Clark are just as relevant to small-sized views of responsive sites as they are to mobile.</p>
<h3 id="provides-a-custom-offline-page">Provides a custom offline page</h3>
<p>When users are offline, keeping them in your PWA provides a more seamless experience than dropping back to the default browser offline page.</p>
<h4 id="why-3">Why <a href="https://web.dev/pwa-checklist/#why-4">#</a></h4>
<p>Users expect installed apps to work regardless of their connection status. A platform-specific app never shows a blank page when it is offline, and a Progressive Web App should never show the browser default offline page. Providing a custom offline experience, both when a user navigates to a URL that hasn’t been cached and when a user tries to use a feature that requires a connection, helps your web experience feel like it’s part of the device it’s running on.</p>
<h4 id="how-3">How <a href="https://web.dev/pwa-checklist/#how-4">#</a></h4>
<p>During a service worker’s <code>install</code> event, you can precache a custom offline page for later use. If a user goes offline, you can respond with the precached custom offline page. You can follow our custom <a href="https://googlechrome.github.io/samples/service-worker/custom-offline-page/">offline page sample</a> to see an example of this in action and learn how to implement it yourself.</p>
<h3 id="is-installable">Is installable</h3>
<p>Users who install or add apps to their device tend to engage with those apps more.</p>
<h4 id="why-4">Why <a href="https://web.dev/pwa-checklist/#why-5">#</a></h4>
<p>Installing a Progressive Web App allows it to look, feel, and behave like all other installed apps. It’s launched from the same place users launch their other apps. It runs in its own app window, separate from the browser, and it appears in the task list, just like other apps.</p>
<p>Why would you want a user to install your PWA? The same reason you’d want a user to install your app from an app store. Users who install your apps are your most engaged audience, and have better engagement metrics than typical visitors, often at parity with app users on mobile devices. These metrics include more repeat visits, longer times on your site and higher conversion rates.</p>
<h4 id="how-4">How <a href="https://web.dev/pwa-checklist/#how-5">#</a></h4>
<p>You can follow our <a href="https://web.dev/customize-install/">installable guide</a> to learn how to make your PWA installable, how to test to see that it’s installable, and try doing it yourself.</p>
<h2 id="optimal-progressive-web-app-checklist">Optimal Progressive Web App checklist <a href="https://web.dev/pwa-checklist/#optimal">#</a></h2>
<p>To create a truly great Progressive Web App, one that feels like a best-in-class app, you need more than just the core checklist. The optimal Progressive Web App checklist is about making your PWA feel like it’s part of the device it’s running on while taking advantage of what makes the web powerful.</p>
<h3 id="provides-an-offline-experience">Provides an offline experience</h3>
<p>Where connectivity isn’t strictly required, your app works the same offline as it does online.</p>
<h4 id="why-5">Why <a href="https://web.dev/pwa-checklist/#why-6">#</a></h4>
<p>In addition to providing a custom offline page, users expect Progressive Web Apps to be usable offline. For example, travel and airline apps should have trip details and boarding passes easily available when offline. Music, video, and podcasting apps should allow for offline playback. Social and news apps should cache recent content so users can read it when offline. Users also expect to stay authenticated when offline, so design for offline authentication. An offline PWA provides a true app-like experience for users.</p>
<h4 id="how-5">How <a href="https://web.dev/pwa-checklist/#how-6">#</a></h4>
<p>After determining which features your users expect to work offline, you’ll need to make your content available and adaptable to offline contexts. In addition, you can use <a href="https://developers.google.com/web/ilt/pwa/working-with-indexeddb">IndexedDB</a>, an in-browser NoSQL storage system, to store and retrieve data, and <a href="https://developers.google.com/web/updates/2015/12/background-sync">background sync</a> to allow users to take actions while offline and defer server communications until the user has a stable connection again. You can also use service workers to store other kinds of content, such as images, video files, and audio files for offline use, as well as use them to implement <a href="https://developers.google.com/web/updates/2016/06/2-cookie-handoff">safe, long-lived sessions</a> to keep users authenticated. From a user experience perspective, you can use <a href="https://uxdesign.cc/what-you-should-know-about-skeleton-screens-a820c45a571a">skeleton screens</a> that give users a perception of speed and content while loading that can then fall back to cached content or an offline indicator as needed.</p>
<h3 id="is-fully-accessible">Is fully accessible</h3>
<p>All user interactions pass <a href="https://www.w3.org/TR/WCAG20/">WCAG 2.0</a> accessibility requirements.</p>
<h4 id="why-6">Why <a href="https://web.dev/pwa-checklist/#why-7">#</a></h4>
<p>Most people at some point in their life will want to take advantage of your PWA in a way that is covered under the <a href="https://www.w3.org/TR/WCAG20/">WCAG 2.0</a> accessibility requirements. Humans’ ability to interact with and understand your PWA exist on a spectrum and needs can be temporary or permanent. By making your PWA accessible, you ensure it’s usable for everyone.</p>
<h4 id="how-6">How <a href="https://web.dev/pwa-checklist/#how-7">#</a></h4>
<p>W3C’s <a href="https://www.w3.org/WAI/fundamentals/accessibility-intro/">Introduction to Web Accessibility</a> is a good place to start. A majority of accessibility testing must be done manually. Tools like the <a href="https://web.dev/lighthouse-accessibility/">Accessibility</a> audits in Lighthouse, <a href="https://github.com/dequelabs/axe-core">axe</a>, and <a href="https://accessibilityinsights.io/">Accessibility Insights</a> can help you automate some accessibility testing. It’s also important to use semantically correct elements instead of recreating those elements on your own, for instance, <code>a</code> and <code>button</code> elements. This ensures that when you do need to build more advanced functionality, accessible expectations are met (such as when to use arrows vs. tabs). <a href="https://accessibilityinsights.io/">A11Y Nutrition Cards</a> has excellent advice on this for some common components.</p>
<h3 id="can-be-discovered-through-search">Can be discovered through search</h3>
<p>Your PWA can be easily <a href="https://web.dev/discoverable/">discovered through search</a>.</p>
<h4 id="why-7">Why <a href="https://web.dev/pwa-checklist/#why-8">#</a></h4>
<p>One of the biggest advantages of the web is the ability to discover sites and apps through search. In fact, <a href="https://www.brightedge.com/resources/research-reports/channel_share">more than half</a> of all website traffic comes from organic search. Making sure that canonical URLs exist for content and that search engines can index your site is critical for users to be able to find your PWA. This is especially true when adopting client-side rendering.</p>
<h4 id="how-7">How <a href="https://web.dev/pwa-checklist/#how-8">#</a></h4>
<p>Start by ensuring that each URL has a unique, descriptive title and meta description. Then you can use the <a href="https://search.google.com/search-console/about">Google Search Console</a> and the <a href="https://web.dev/lighthouse-seo/">Search Engine Optimization audits</a> in Lighthouse to help you debug and fix discoverability issues with your PWA. You can also use <a href="https://www.bing.com/toolbox/webmaster">Bing</a>’s or <a href="https://webmaster.yandex.com/welcome/">Yandex</a>’s webmaster tools, and consider including <a href="https://goo.gle/search-gallery">structured data</a> via schemas from <a href="https://schema.org/">Schema.org</a> in your PWA.</p>
<h3 id="works-with-any-input-type">Works with any input type</h3>
<p>Your PWA is equally usable with a mouse, a keyboard, a stylus, or touch.</p>
<h4 id="why-8">Why <a href="https://web.dev/pwa-checklist/#why-9">#</a></h4>
<p>Devices offer a variety of input methods, and users should be able to seamlessly switch between them while using your application. Just as important, input methods should not depend on screen size, meaning that large viewports need to support touch and small viewports need to support keyboards and mice. To the best of your ability, ensure that your application and all of its features supports usage of any input method your user may choose to use. Where appropriate, you should also enhance experiences to allow for input-specific controls as well (such as pull-to-refresh).</p>
<h4 id="how-8">How <a href="https://web.dev/pwa-checklist/#how-9">#</a></h4>
<p>The <a href="https://developers.google.com/web/updates/2016/10/pointer-events">Pointer Events API</a> provides a unified interface for working with various input options, and is especially good for adding stylus support. For supporting both touch and keyboard, ensure that you’re using the correct semantic elements (anchors, buttons, form controls, etc.) and not rebuilding these with non-semantic HTML (which is good for accessibility). When including interactions that activate on hover, ensure they can activate on click or tap, too.</p>
<h3 id="provides-context-for-permission-requests">Provides context for permission requests</h3>
<p>When asking permission to use powerful APIs, provide context and ask only when the API is needed.</p>
<h4 id="why-9">Why <a href="https://web.dev/pwa-checklist/#why-10">#</a></h4>
<p>APIs that trigger a permission prompt, like notifications, geolocation, and credentials, are intentionally designed to be disruptive to a user because they tend to be related to powerful functionality that requires opt-in. Triggering these prompts without additional context, like on page load, makes users less likely to accept those permissions and more likely to distrust them in the future. Instead, only trigger those prompts after providing an in-context rationale to the user for why you need that permission.</p>
<h4 id="how-9">How <a href="https://web.dev/pwa-checklist/#how-10">#</a></h4>
<p>The <a href="https://developers.google.com/web/fundamentals/push-notifications/permission-ux">Permission UX</a> article and UX Planet’s <a href="https://uxplanet.org/mobile-ux-design-the-right-ways-to-ask-users-for-permissions-6cdd9ab25c27">The Right Ways to Ask Users for Permissions</a> are good resources to understand how to design permission prompts that, while focused on mobile, apply to all PWAs.</p>
<h3 id="follows-best-practices-for-healthy-code">Follows best practices for healthy code</h3>
<p>Keeping your codebase healthy makes it easier to meet your goals and deliver new features.</p>
<h4 id="why-10">Why <a href="https://web.dev/pwa-checklist/#why-11">#</a></h4>
<p>There’s a lot that goes into building a modern web application. Keeping your application up-to-date and your codebase healthy makes it easier for you to deliver new features that meet the other goals laid out in this checklist.</p>
<h4 id="how-10">How <a href="https://web.dev/pwa-checklist/#how-11">#</a></h4>
<p>There are a number of high-priority checks to ensure a healthy codebase: avoiding using libraries with known vulnerabilities, ensuring you’re not using deprecated APIs, removing web anti-patterns from your codebase (like using <code>document.write()</code> or having non-passive scroll event listeners), and even coding defensively to ensure your PWA doesn’t break if analytics or other third party libraries fail to load. Consider requiring static code analysis, like linting, as well as automated testing, in multiple browsers and release channels. These techniques can help catch errors before they make it into production.</p>
<p><a href="https://web.dev/tags/progressive-web-apps/">Progressive Web Apps</a></p>
<p>Last updated: Feb 24, 2020 <a href="https://github.com/GoogleChrome/web.dev/blob/master/src/site/content/en/progressive-web-apps/pwa-checklist/index.md">Improve article</a></p>
<h2 id="give-feedback">Give feedback</h2>
<p>All fields optional</p>
<p>Yes</p>
<p>No</p>
<p>Was this page helpful?</p>
<p>Did this page help you complete your goal(s)?</p>
<p>Congrats on finding this field, I’d recommend you not filling it out though… </p>
<p>SUBMIT</p>
<p><a href="https://web.dev/progressive-web-apps">Return to all articles</a></p>
<ul>
<li><h3 id="contribute">Contribute</h3>
<ul>
<li><a href="https://github.com/GoogleChrome/web.dev/issues/new?assignees=&amp;labels=bug&amp;template=bug_report.md&amp;title=">File a bug</a></li>
<li><a href="https://github.com/googlechrome/web.dev">View source</a></li>
</ul></li>
<li><h3 id="related-content">Related content</h3>
<ul>
<li><a href="https://blog.chromium.org/">Chrome updates</a></li>
<li><a href="https://developers.google.com/web/">Web Fundamentals</a></li>
<li><a href="https://developers.google.com/web/showcase/">Case studies</a></li>
<li><a href="https://devwebfeed.appspot.com/">DevWeb Content Firehose</a></li>
<li><a href="https://web.dev/podcasts/">Podcasts</a></li>
<li><a href="https://web.dev/shows/">Shows</a></li>
</ul></li>
<li><h3 id="connect">Connect</h3>
<ul>
<li><a href="https://www.twitter.com/ChromiumDev">Twitter</a></li>
<li><a href="https://www.youtube.com/user/ChromeDevelopers">YouTube</a></li>
</ul></li>
</ul>
<p><a href="https://developers.google.com/"><img src="https://web.dev/images/lockup-color.png" alt="Google Developers" /></a></p>
<ul>
<li><a href="https://developer.chrome.com/">Chrome</a></li>
<li><a href="https://firebase.google.com/">Firebase</a></li>
<li><a href="https://cloud.google.com/">Google Cloud Platform</a></li>
<li><a href="https://developers.google.com/products">All products</a></li>
</ul>
<p>Choose language                                   ENGLISH (en)                             </p>
<ul>
<li><a href="https://policies.google.com/">Terms &amp; Privacy</a></li>
<li><a href="https://web.dev/community-guidelines/">Community Guidelines</a></li>
</ul>
<p>Except as otherwise noted, the content of this page is licensed under the <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 License</a>, and code samples are licensed under the <a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0 License</a>. For details, see the <a href="https://developers.google.com/terms/site-policies">Google Developers Site Policies</a>.</p>
</body>
</html>
